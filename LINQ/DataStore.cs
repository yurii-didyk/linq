﻿using System;
using System.Collections.Generic;
using System.Text;
using LINQ.Models;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using Newtonsoft.Json;

namespace LINQ
{
    class DataStore
    {
        //private readonly string URL = "http://localhost:50351/api";
        //public List<Project> Projects { get; set; }
        //public List<Task> Tasks { get; set; }
        //public List<Team> Teams { get; set; }
        //public List<User> Users { get; set; }
        //public List<TaskStateModel> TaskStates { get; set; }

        //public DataStore()
        //{
        //    Projects = JsonConvert.DeserializeObject<List<Project>>(GetHttpResponse($"{URL}/projects").Content.ReadAsStringAsync().Result);
        //    Tasks = JsonConvert.DeserializeObject<List<Task>>(GetHttpResponse($"{URL}/tasks").Content.ReadAsStringAsync().Result);
        //    Teams = JsonConvert.DeserializeObject<List<Team>>(GetHttpResponse($"{URL}/teams").Content.ReadAsStringAsync().Result);
        //    Users = JsonConvert.DeserializeObject<List<User>>(GetHttpResponse($"{URL}/users").Content.ReadAsStringAsync().Result);
        //    TaskStates = JsonConvert.DeserializeObject<List<TaskStateModel>>(GetHttpResponse($"{URL}/taskstates").Content.ReadAsStringAsync().Result);
        //}

        public static HttpResponseMessage GetHttpResponse(string url)
        {
            HttpResponseMessage response;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                response = client.GetAsync(url).Result;
            }
            return response;
        }
        public static HttpResponseMessage PostHttpResponse(string url, object obj)
        {
            HttpResponseMessage response;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                response = client.PostAsJsonAsync(url, obj).Result;
            }
            return response;
        }
    }
}

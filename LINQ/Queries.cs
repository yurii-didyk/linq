﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net.Http;
using LINQ.Models;

namespace LINQ
{
    class Queries
    {
        private static string URL = "http://localhost:50351/api";
        //private static DataStore data;
        //public Queries()
        //{
        //    data = new DataStore();
        //}
        // 1 querry
        public static List<TaskCountForProject> TasksCountForProject(int userId)
        {
            var result = JsonConvert.DeserializeObject<List<TaskCountForProject>>(DataStore.GetHttpResponse($"{URL}/queries/1/{userId}").Content.ReadAsStringAsync().Result);
            return result;
            
        }
        // 2 querry
        public static List<Task> TasksForPerformer(int userId)
        {
            var result = JsonConvert.DeserializeObject<List<Task>>(DataStore.GetHttpResponse($"{URL}/queries/2/{userId}").Content.ReadAsStringAsync().Result);
            return result;
        }
        // 3 querry

        public static List<Tuple<int, string>> FinishedTasksInThisYear(int userId)
        { 
            var result = JsonConvert.DeserializeObject<List<Tuple<int,string>>>(DataStore.GetHttpResponse($"{URL}/queries/3/{userId}").Content.ReadAsStringAsync().Result);
            return result;
        }
        // 4 querry

        public static List<Tuple<int, string, List<User>>> TeamsOlder12()
        {
            var result = JsonConvert.DeserializeObject<List<Tuple<int, string, List<User>>>>(DataStore.GetHttpResponse($"{URL}/queries/4").Content.ReadAsStringAsync().Result);
            return result;
        }

        // 5 querry 
        public static List<Tuple<User, List<Task>>> GetSortedUsers()
        {
            var result = JsonConvert.DeserializeObject<List<Tuple<User, List<Task>>>>(DataStore.GetHttpResponse($"{URL}/queries/5").Content.ReadAsStringAsync().Result);
            return result;
        }
        // 6 querry
        public static HelperModel1 GetModel1(int userId)
        {
            var result = JsonConvert.DeserializeObject<HelperModel1>(DataStore.GetHttpResponse($"{URL}/queries/6/{userId}").Content.ReadAsStringAsync().Result);
            return result;
        }
        // 7 querry 
        public static HelperModel2 GetModel2(int projectId)
        {
            var result = JsonConvert.DeserializeObject<HelperModel2>(DataStore.GetHttpResponse($"{URL}/queries/7/{projectId}").Content.ReadAsStringAsync().Result);
            return result;
        }
    }
}
